var Customer = require("../models/customer");
var config = require("../../config");
var superSecret = config.secret;
var jwt = require("jsonwebtoken");
exports.Login = function(req, res, next) {
    console.log("authenticate thu test",req.body);
    Customer.findOne({
      name: req.body.name,
    })
      .select("name username password")
      .exec(function(err, user) {
        if (err) throw err;

        //no user with that username was found
        if (!user) {
          res.json({
            sucess: false,
            message: "Authentication failed.User not found.",
          });
        } else if (user) {
          var valiPassword = user.comparePassword(req.body.password);
          if (!valiPassword) {
            res.json({
              success: false,
              message: "Authentication failed. Wrong password.",
            });
          } else {
            // if user found and password is right
            //create token
            var token = jwt.sign(
              {
                name: user.name,
                //   username:user.username
              },
              superSecret,
              {
                expiresIn: "24h", // expires in 24h
              }
            );
            res.json({
              sucess: true,
              message: "Lam viec voi token",
              token: token,
            });
          }
        }
      });
}