const express = require('express');
const authenticate=require('../middlewares/authenticate');

var router = express.Router();

var customerController = require('../controllers/customer.controller');

router.get('/get/:id', customerController.getCustomerByID);
router.get('/get', customerController.getCustomer);
router.post('/create',customerController.createCustomer);
router.put('/edit/:id', customerController.editCustomer);
router.delete('/delete/:id', customerController.deleteCustomerByID);

module.exports = router;