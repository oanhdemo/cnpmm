var mongoose = require('mongoose');
var Schema=mongoose.Schema;
var bcrypt = require('bcrypt-nodejs');
// mongoose.connect('mongodb+srv://Nhocga123:Nhocga123@cluster0.u7vq9.mongodb.net/DemoMongo?retryWrites=true&w=majority', {
//     useNewUrlParser: true,
//     useUnifiedTopology: true
// });
var CustomerSchema=new Schema({
    name: {type:String ,required:true},
    password:{type:String},
    phoneNumber: {type:String ,required:true, index:{unique:true}},
    address: {type:String ,required:true},
    job: {type:String},
})

CustomerSchema.methods.comparePassword = function(tempPassword) {
    const user = this;
    if(user.password.trim() == tempPassword.trim())
    {
        return true;
    }
    else {
        return false;
    }

}

module.exports=mongoose.model('Customer',CustomerSchema);