import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { ManagementComponent } from './management/management.component';
import { UserComponent } from './management/user/user.component';


const routes: Routes = [
  {path: "", redirectTo:"login",pathMatch: "full"},
  {path: "login", component: LoginComponent},
  {path: "management", component: ManagementComponent,
    children: [{path: "user", component: UserComponent}]
  }
  
]

@NgModule({
      imports: [RouterModule.forRoot(routes)],
      exports: [RouterModule]
    })
export class AppRoutingModule { }
