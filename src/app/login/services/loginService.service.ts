import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { FormBuilder } from '@angular/forms';
import { ApiUrl } from 'src/app/app.component';

@Injectable({
  providedIn: 'root'
})

export class loginService {
  url = ApiUrl;

  constructor(private http: HttpClient, private fb: FormBuilder) { }

  Save(response) {
    console.log(response);
  }

  saveResponse(response){
    return this.http.post(this.url + '/ApplicationsUser/LoginSocial',response);
  }

  roleMatch(allowedRoled): boolean {
    var isMatch = false;
    var payload = JSON.parse(window.atob(localStorage.getItem('token').split('.')[1]));
    var userRole = payload.role;
    allowedRoled.forEach(element => {
      if (userRole == element) {
        isMatch = true;
        return false;
      }
    });
    return isMatch;
  }
  login(formData) {
    var data ={
        name:formData.UserName,
        password:formData.Password
    }
    return this.http.post(this.url + '/authenticates/login', data);
  }

//   forgotPassword(formData) {
//     return this.http.post(this.url + '/ApplicationsUser/forgetPassWord?email='+formData,null);
//   }
}