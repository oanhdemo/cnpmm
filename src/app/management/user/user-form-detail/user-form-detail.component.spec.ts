import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserFormDetailComponent } from './user-form-detail.component';

describe('UserFormDetailComponent', () => {
  let component: UserFormDetailComponent;
  let fixture: ComponentFixture<UserFormDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserFormDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserFormDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
