import { Component } from '@angular/core';

export const ApiUrl="http://localhost:8080";
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Angular';
}
